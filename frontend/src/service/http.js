import axios from "axios";
import { Loading, Message } from "element-ui";
import router from "../router";
import store from "../store";

let loading;

function startLoading() {
  loading = Loading.service({
    lock: true,
    text: "載入中....",
    background: "rgba(0, 0, 0, 0.7)",
  });
}

function endLoading() {
  loading.close();
}

axios.defaults.withCredentials = true;
axios.defaults.baseURL = "http://127.0.0.1:8000/";
// axios.defaults.baseURL = "http://192.168.0.123:8000/";
// axios.defaults.baseURL = "http://192.168.0.124:8000/";
// axios.defaults.baseURL = 'http://0.0.0.0:80/';

// 請求攔截
axios.interceptors.request.use(
  (confing) => {
    startLoading();
    //設定請求頭
    const token = store.getters.getToken;
    if (store.getters.isAuthenticated) {
      // console.log(token)
      confing.headers.Authorization = `Bearer ${token}`;
    }
    return confing;
  },
  (error) => {
    return Promise.reject(error);
  }
);

//響應攔截
axios.interceptors.response.use(
  (response) => {
    endLoading();
    return response;
  },
  (error) => {
    Message.error(error.response.data);
    endLoading();

    // 獲取狀態碼
    const { status } = error.response;

    if (status === 401) {
      Message.error("請重新登入");
      //clear token
      this.$store.dispatch("LogOut");
      //return to login page
      router.push("/login");
    }
    if (status === 400) {
      Message.error("You are not authorized");
      //clear token
      this.$store.dispatch("LogOut");
      //return to login page
      router.push("/login");
    }
    return Promise.reject(error);
  }
);

export default axios;
