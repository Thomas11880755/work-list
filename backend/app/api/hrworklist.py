from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session
from typing import List

from ..schemas.hrworklist import Hrworklists, HrworklistCreate, HrworklistUpdate
from ..schemas.allfull import HrWorkliseFull
from ..repository.hrworklist_crud import (create_new_hrworklist,
                                          retreive_hrworklist,
                                          list_hrworklists,
                                          update_hrworklist_by_id,
                                          delete_hrworklist_by_id)
from ..database import get_db
from ..auth import login_manager

router = APIRouter(
    prefix="/hrworklist",
    tags=["Hrworklist"],
)


@router.post("/create-hrworklist", response_model=Hrworklists)
def create_hrworklist(hrworklist: HrworklistCreate, db: Session = Depends(get_db), user=Depends(login_manager)):
    hrworklist.user_id = user.id
    hrworklist = create_new_hrworklist(
        hrworklist=hrworklist, db=db)
    return hrworklist


@router.get("/get/{id}", response_model=Hrworklists)
def retreive_hrworklist_by_id(id: int, db: Session = Depends(get_db)):
    hrworklist = retreive_hrworklist(id=id, db=db)
    if not hrworklist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"hrworklist with id {id} does not exist")
    return hrworklist


@router.get('/my/{user_id}', response_model=List[HrWorkliseFull])
def read_hrworklist_my(user_id: int, db: Session = Depends(get_db), user=Depends(login_manager)):
    list_dp_p = user.checklistAll_permission
    is_superuser = user.is_superuser
    if list_dp_p == 2 or list_dp_p == 3 or is_superuser == True:
        hrworklists = list_hrworklists(db, user_id=user_id)
        return hrworklists
    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                        detail="You are not permitted!!")


@router.get("/hrworklists-all", response_model=List[HrWorkliseFull])
def retreive_all_hrworklists(db: Session = Depends(get_db), user=Depends(login_manager)):
    user_id = user.id
    return list_hrworklists(db, user_id)


@router.put("/update/{id}")
def edit_workhour(id: int, hrworklist_items: HrworklistUpdate, db: Session = Depends(get_db), user=Depends(login_manager)):
    user_id = user.id
    hrworklist_retrieved = retreive_hrworklist(
        db=db, id=id)
    if not hrworklist_retrieved:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"hrworklist with id {id} does not exist")
    if hrworklist_retrieved.user_id == user.id:
        message = update_hrworklist_by_id(
            id=id, hrworklist_items=hrworklist_items, db=db, user_id=user_id)
    else:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail=f"You are not authorized to update.")
    return {"detail": "hrworklist has been updated."}


@router.delete("/delete/{id}")
def delete_hrworklist(id: int, db: Session = Depends(get_db)):
    return delete_hrworklist_by_id(id, db)
