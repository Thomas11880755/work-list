from sqlalchemy.orm import Session
from fastapi import HTTPException, status
from ..schemas.hrworklist import HrworklistCreate, HrworklistUpdate
from ..models import HrWorklist


def create_new_hrworklist(db: Session, hrworklist: HrworklistCreate):
    db_workhour = HrWorklist(
        user_id=hrworklist.user_id,
        start_date=hrworklist.start_date,
        end_date=hrworklist.end_date,
        description=hrworklist.description,
    )
    db.add(db_workhour)
    db.commit()
    db.refresh(db_workhour)
    return db_workhour


def retreive_hrworklist(id: int, db: Session):
    hrworklist = db.query(HrWorklist).filter(HrWorklist.id == id).first()
    return hrworklist


def list_hrworklists(db: Session, user_id: int):
    return db.query(HrWorklist).filter(HrWorklist.user_id == user_id).all()


def update_hrworklist_by_id(id: int, hrworklist_items: HrworklistUpdate, db: Session, user_id: int):
    existing_hrworklist = db.query(HrWorklist).filter(HrWorklist.id == id)
    if not existing_hrworklist.first():
        return 0
    hrworklist_items.__dict__.update(user_id=user_id)
    existing_hrworklist.update(hrworklist_items.__dict__)
    db.commit()
    return 1


def delete_hrworklist_by_id(id: int, db: Session):
    hrworklist_items = db.query(HrWorklist).filter(
        HrWorklist.id == id)

    if not hrworklist_items.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Hrworklist with id {id} not found")

    hrworklist_items.delete(synchronize_session=False)
    db.commit()
    return "hrworklist_items hase being delete"
