from sqlalchemy.orm import Session
from sqlalchemy import text, func
from sqlalchemy.sql.expression import false, true
from fastapi import HTTPException, status
# from .. import models, schemas

from ..models import Workhour
from ..schemas import whorkhours


def get_workhour(db: Session, workhour_id: int):
    return db.query(Workhour).order_by(text("date desc")).filter(
        Workhour.id == workhour_id).first()


def get_workhours(db: Session, user_id: int, skip: int = 0, limit: int = 100):
    return db.query(Workhour).order_by(text("date desc")).filter(
        Workhour.user_id == user_id).filter(
        Workhour.admin1_checked == "false").offset(skip).limit(limit).all()


def get_total_workhours(db: Session, skip: int = 0):
    return db.query(Workhour).order_by(text("date desc")).offset(skip).all()


# def get_workhours_by_user_id(db: Session, user_id, skip: int = 0, limit: int = 100):
#     return db.query(Workhour).order_by(text("date desc")).filter(
#         Workhour.user_id == user_id).offset(skip).limit(limit).all()


def get_monthlyworkhours_by_user_id(db: Session, user_id):
    qry = (db.query(
        func.to_char(Workhour.date,
                     'YYYY-MM').label('year_month'),
        func.sum(Workhour.hour).label('total_hour'),
        func.sum(Workhour.overtime_hour).label(
            'total_overtime_hour')
    )
        .order_by(text("year_month desc"))
        # optionally check only last 2 month data (could have partial months)
        .filter(Workhour.user_id == user_id)
        .group_by(
        func.to_char(Workhour.date, 'YYYY-MM').label('year_month'),
    )
        .all()
    )
    return qry


def get_workhours_by_task_id(db: Session, task_id: int, skip: int = 0, limit: int = 100):
    return db.query(Workhour).order_by(text("date desc")).filter(
        Workhour.task_id == task_id).offset(skip).limit(limit).all()


def get_workhours_by_user_task(db: Session, user_id: int, task_id: int, skip: int = 0, limit: int = 100):
    return db.query(Workhour).order_by(text("date desc")).filter(
        Workhour.user_id == user_id, Workhour.task_id == task_id).offset(
            skip).limit(limit).all()


def create_workhour(db: Session, workhour_items: whorkhours.WorkhourCreate):
    db_workhour = Workhour(
        user_id=workhour_items.user_id,
        task_id=workhour_items.task_id,
        date=workhour_items.date,
        hour=workhour_items.hour,
        description=workhour_items.description,
        is_overtime=workhour_items.is_overtime,
        overtime_hour=workhour_items.overtime_hour
    )
    db.add(db_workhour)
    db.commit()
    db.refresh(db_workhour)
    return db_workhour


def update_workhour(workhour_id: int, workhour_items: whorkhours.WorkhourUpdate, db: Session):
    db_workhour = db.query(Workhour).filter(
        Workhour.id == workhour_id)

    if not db_workhour.first():
        return 0
    workhour_items.__dict__.update()
    db_workhour.update(workhour_items.__dict__)
    db.commit()
    return 1


def delete_workhour(id: int, db: Session):
    workhour_items = db.query(Workhour).filter(
        Workhour.id == id)

    if not workhour_items.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Expen with id {id} not found")

    workhour_items.delete(synchronize_session=False)
    db.commit()
    return "Workhour hase being delete"
