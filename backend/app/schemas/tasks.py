from typing import List, Optional
from pydantic import BaseModel
import datetime
from .users import User


class WorkhourTaskFull(BaseModel):
    user_id: Optional[int] = None
    task_id: Optional[int] = None
    date: datetime.date
    hour: float
    description: Optional[str] = None
    is_overtime: Optional[bool] = False
    overtime_hour: float
    user: Optional[User]

    class Config:
        orm_mode = True


class TaskBase(BaseModel):
    taskname: str
    fullname: str
    organization: str


class TaskCreate(TaskBase):
    pass


class TaskUpdate(TaskBase):
    pass


class Task(TaskBase):
    id: int

    class Config:
        orm_mode = True


class TaskGYBase(BaseModel):
    id: str
    taskname: str
    workhours: List[WorkhourTaskFull] = []

    class Config:
        orm_mode = True


class TaskByUserTotal(BaseModel):
    year_month: str
    taskname: str
    username: str
    total_hour: float

    class Config:
        orm_mode = True
